use std::io::prelude::*;
pub fn main(input: impl BufRead) {
    let mut sum = 0;
    for line in input.lines().map(Result::unwrap) {
        let mut nums = line.split(|ch| ch == 'x')
                           .map(|num| num.parse::<i32>().unwrap())
                           .collect::<Vec<_>>();
        nums[..].sort();
        sum += 3 * nums[0] * nums[1] + 2 * nums[1] * nums[2] + 2 * nums[2] * nums[0];
    }
    println!("{}", sum);
}
