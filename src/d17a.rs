use std::io::prelude::*;
use std::collections::HashMap;
pub fn main(input: impl BufRead) {
    let mut arr = vec![];
    for line in input.lines().map(Result::unwrap) {
        arr.push(line.parse::<i32>().unwrap());
    }

    let mut memo = Memo::new();
    println!("{}", memo.solve(&arr, 150));
}

// This is a counting version of the subset sum problem, which is NP-hard.
// Dynamic programming using top-down memoization speeds this up significantly.
struct Memo {
    memo: HashMap<(usize, i32), u32>,
}
impl Memo {
    fn new() -> Self {
        Self { memo: HashMap::new() }
    }
    fn solve(&mut self, arr: &[i32], sum: i32) -> u32 {
        if sum == 0 {
            return 1;
        }
        if arr.is_empty() {
            return 0;
        }
        if let Some(ans) = self.memo.get(&(arr.len(), sum)) {
            return *ans;
        }

        let take = self.solve(&arr[1..], sum - arr[0]);
        let leave = self.solve(&arr[1..], sum);
        self.memo.insert((arr.len(), sum), take + leave);
        take + leave
    }
}
