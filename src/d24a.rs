use std::io::prelude::*;
use std::collections::HashMap;
pub fn main(input: impl BufRead) {
    let mut set = vec![];
    let mut total = 0;
    for line in input.lines().map(std::result::Result::unwrap) {
        set.push(line.parse::<i64>().unwrap());
        total += line.parse::<i64>().unwrap();
    }
    let part_sum = total / 3;

    let mut memo = Memo::new();
    let result = memo.solve(&set, part_sum, part_sum).unwrap();
    let mut parts = [
        (result.part0_size, result.part0_prod),
        (result.part1_size, result.part1_prod),
        (result.part2_size, result.part2_prod),
    ];
    parts.sort();
    println!("{}", parts[0].1);
}

// Dynamic programming solution to 3-way partition problem.
//
// This builds off of the common pseudo-polynomial DP solution to the partition
// problem, extending it to partition into 3 sets of equal sum.
//
// Recurrence relation:
// Let f(i, n, m) iff there are two disjoint sets in S[0,i) that sum to n and m.
// Then f(i, n, m) = f(i-1, n, m) || f(i-1, n-S[i], m) || f(i-1, n, m-S[i]).
//
// Rather than simply the decision problem, this also returns the solution with
// the smallest individual partition, then by said partition's product.
struct Memo {
    memo: HashMap<(usize, i64, i64), Option<Result>>,
}
impl Memo {
    fn new() -> Self {
        Self { memo: HashMap::new() }
    }
    fn solve(&mut self, set: &[i64], sum1: i64, sum2: i64) -> Option<Result> {
        if set.is_empty() || sum1 < 0 || sum2 < 0 {
            if sum1 == 0 && sum2 == 0 {
                Some(Result::new())
            } else {
                None
            }
        } else if let Some(&sol) = self.memo.get(&(set.len(), sum1, sum2)) {
            sol
        } else {
            let i = set.len() - 1;
            let mut results = vec![];
            if let Some(mut result) = self.solve(&set[..i], sum1, sum2) {
                result.part0_size += 1;
                result.part0_prod = result.part0_prod.saturating_mul(set[i]);
                results.push(result);
            }
            if let Some(mut result) = self.solve(&set[..i], sum1 - set[i], sum2) {
                result.part1_size += 1;
                result.part1_prod = result.part1_prod.saturating_mul(set[i]);
                results.push(result);
            }
            if let Some(mut result) = self.solve(&set[..i], sum1, sum2 - set[i]) {
                result.part2_size += 1;
                result.part2_prod = result.part2_prod.saturating_mul(set[i]);
                results.push(result);
            }
            results.sort();

            let result = results.get(0).map(|&res| res);
            self.memo.insert((set.len(), sum1, sum2), result);
            result
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
struct Result {
    part0_size: usize,
    part0_prod: i64,
    part1_size: usize,
    part1_prod: i64,
    part2_size: usize,
    part2_prod: i64,
}
impl Result {
    fn new() -> Self {
        Self {
            part0_size: 0, part0_prod: 1,
            part1_size: 0, part1_prod: 1,
            part2_size: 0, part2_prod: 1,
        }
    }
}
impl std::cmp::Ord for Result {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        let mut lhs = [
            (self.part0_size, self.part0_prod),
            (self.part1_size, self.part1_prod),
            (self.part2_size, self.part2_prod),
        ];
        lhs.sort();
        let mut rhs = [
            (other.part0_size, other.part0_prod),
            (other.part1_size, other.part1_prod),
            (other.part2_size, other.part2_prod),
        ];
        rhs.sort();

        lhs.cmp(&rhs)
    }
}
impl std::cmp::PartialOrd for Result {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}
