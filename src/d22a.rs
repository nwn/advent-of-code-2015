use std::io::prelude::*;
pub fn main(input: impl BufRead) {
    let player = Player {
        health: 50,
        mana: 500,
    };
    let enemy = {
        let mut lines = input.lines().map(Result::unwrap);
        Enemy {
            health: lines.next().unwrap().split_whitespace().last().unwrap().parse().unwrap(),
            damage: lines.next().unwrap().split_whitespace().last().unwrap().parse().unwrap(),
        }
    };
    let effects = Effects {
        shield: 0,
        poison: 0,
        recharge: 0,
    };

    println!("{}", player_turn(player, enemy, effects).unwrap());
}

#[derive(Copy, Clone, Debug)]
struct Player {
    health: i16,
    mana: i16,
}
#[derive(Copy, Clone, Debug)]
struct Enemy {
    health: i16,
    damage: i16,
}
#[derive(Copy, Clone, Debug)]
struct Effects {
    shield: u8,
    poison: u8,
    recharge: u8,
}

fn player_turn(mut player: Player, mut enemy: Enemy, mut effects: Effects) -> Option<i16> {
    // Activate all effects
    if effects.shield > 0 { effects.shield -= 1; }
    if effects.poison > 0 { enemy.health -= 3; effects.poison -= 1; }
    if effects.recharge > 0 { player.mana += 101; effects.recharge -= 1; }

    // Check end conditions
    if player.health <= 0 { return None; }
    if enemy.health <= 0 { return Some(0); }

    // Player attacks
    let mut branches = vec![];
    let (missile_cost, drain_cost, shield_cost, poison_cost, recharge_cost) = (53, 73, 113, 173, 229);
    if player.mana >= missile_cost {
        let (mut player, mut enemy, effects) = (player, enemy, effects);
        player.mana -= missile_cost;
        enemy.health -= 4;
        branches.push((player, enemy, effects, missile_cost));
    }
    if player.mana >= drain_cost {
        let (mut player, mut enemy, effects) = (player, enemy, effects);
        player.mana -= drain_cost;
        player.health += 2;
        enemy.health -= 2;
        branches.push((player, enemy, effects, drain_cost));
    }
    if effects.shield == 0 && player.mana >= shield_cost {
        let (mut player, enemy, mut effects) = (player, enemy, effects);
        player.mana -= shield_cost;
        effects.shield = 6;
        branches.push((player, enemy, effects, shield_cost));
    }
    if effects.poison == 0 && player.mana >= poison_cost {
        let (mut player, enemy, mut effects) = (player, enemy, effects);
        player.mana -= poison_cost;
        effects.poison = 6;
        branches.push((player, enemy, effects, poison_cost));
    }
    if effects.recharge == 0 && player.mana >= recharge_cost {
        let (mut player, enemy, mut effects) = (player, enemy, effects);
        player.mana -= recharge_cost;
        effects.recharge = 5;
        branches.push((player, enemy, effects, recharge_cost));
    }

    let mut least_cost = None;
    for (player, enemy, effects, spent) in branches {
        if let Some(cost) = enemy_turn(player, enemy, effects) {
            if least_cost.is_none() || spent + cost < least_cost.unwrap() {
                least_cost = Some(spent + cost);
            }
        }
    }
    least_cost
}

fn enemy_turn(mut player: Player, mut enemy: Enemy, mut effects: Effects) -> Option<i16> {
    // Activate all effects
    let mut armour = 0;
    if effects.shield > 0 { armour += 7; effects.shield -= 1; }
    if effects.poison > 0 { enemy.health -= 3; effects.poison -= 1; }
    if effects.recharge > 0 { player.mana += 101; effects.recharge -= 1; }

    // Enemy attacks
    player.health -= std::cmp::max(1, enemy.damage - armour);

    // Check end conditions
    if enemy.health <= 0 { return Some(0); }
    if player.health <= 0 { return None; }

    player_turn(player, enemy, effects)
}
