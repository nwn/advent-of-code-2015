use std::io::prelude::*;
pub fn main(input: impl BufRead) {
    let mut reindeer = vec![];
    for line in input.lines().map(Result::unwrap) {
        let words: Vec<_> = line.split_whitespace().collect();
        let speed = words[3].parse::<i32>().unwrap();
        let travel = words[6].parse::<i32>().unwrap();
        let rest = words[13].parse::<i32>().unwrap();

        reindeer.push(Reindeer{ speed, travel, rest });
    }

    let mut points = vec![0; reindeer.len()];
    let mut pos = vec![0; reindeer.len()];
    for t in 0..2503 {
        for i in 0..reindeer.len() {
            let reindeer = &reindeer[i];
            if t % (reindeer.travel + reindeer.rest) < reindeer.travel {
                pos[i] += reindeer.speed;
            }
        }

        let max = pos.iter().max().unwrap();
        for i in 0..reindeer.len() {
            if pos[i] == *max {
                points[i] += 1;
            }
        }
    }

    println!("{}", points.iter().max().unwrap());
}

struct Reindeer {
    speed: i32,
    travel: i32,
    rest: i32,
}
