use std::io::prelude::*;
use std::collections::HashMap;
use std::ops::Add;
pub fn main(input: impl BufRead) {
    let player = Character {
        health: 100,
        damage: 0,
        armour: 0,
    };
    let enemy = {
        let mut lines = input.lines().map(Result::unwrap);
        let health = lines.next().unwrap().split_whitespace().last().unwrap().parse::<i32>().unwrap();
        let damage = lines.next().unwrap().split_whitespace().last().unwrap().parse::<i32>().unwrap();
        let armour = lines.next().unwrap().split_whitespace().last().unwrap().parse::<i32>().unwrap();

        Character { health, damage, armour }
    };

    let weapons = [
        Item { cost:  8, damage: 4, armour: 0 },
        Item { cost: 10, damage: 5, armour: 0 },
        Item { cost: 25, damage: 6, armour: 0 },
        Item { cost: 40, damage: 7, armour: 0 },
        Item { cost: 74, damage: 8, armour: 0 },
    ];
    let armours = [
        Item { cost:  13, damage: 0, armour: 1 },
        Item { cost:  31, damage: 0, armour: 2 },
        Item { cost:  53, damage: 0, armour: 3 },
        Item { cost:  75, damage: 0, armour: 4 },
        Item { cost: 102, damage: 0, armour: 5 },
    ];
    let rings = [
        Item { cost:  25, damage: 1, armour: 0 },
        Item { cost:  50, damage: 2, armour: 0 },
        Item { cost: 100, damage: 3, armour: 0 },
        Item { cost:  20, damage: 0, armour: 1 },
        Item { cost:  40, damage: 0, armour: 2 },
        Item { cost:  80, damage: 0, armour: 3 },
    ];
    let nothing = Item { cost: 0, damage: 0, armour: 0 };

    let mut battle = Battle::new();
    let mut best = 1_000_000;
    for w in 0..weapons.len() {
        let weapon = weapons[w];
        for a in 0..=armours.len() {
            let armour = if a < armours.len() { armours[a] } else { nothing };
            for r1 in 0..=rings.len() {
                let ring1 = if r1 < rings.len() { rings[r1] } else { nothing };
                for r2 in r1+1..=rings.len() {
                    let ring2 = if r2 < rings.len() { rings[r2] } else { nothing };

                    let items = weapon + armour + ring1 + ring2;
                    if battle.battle(player + items, enemy) {
                        let cost = items.cost;
                        if cost < best {
                            best = cost;
                        }
                    }
                }
            }
        }
    }
    println!("{}", best);
}

struct Battle {
    memo: HashMap<Character, bool>,
}
impl Battle {
    fn new() -> Self {
        Self { memo: HashMap::new() }
    }
    fn battle(&mut self, mut player: Character, mut enemy: Character) -> bool {
        if let Some(result) = self.memo.get(&player) {
            return *result;
        }
        let entry = self.memo.entry(player);

        for i in 0.. {
            let (attacker, defender) = if i % 2 == 0 {
                (&mut player, &mut enemy)
            } else {
                (&mut enemy, &mut player)
            };

            defender.health -= std::cmp::max(1, attacker.damage - defender.armour);

            if defender.health <= 0 {
                // Defender loses; if that's the enemy, the player wins!
                return *entry.or_insert(i % 2 == 0);
            }
        }
        unreachable!();
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
struct Character {
    health: i32,
    damage: i32,
    armour: i32,
}
impl Add<Item> for Character {
    type Output = Self;
    fn add(self, item: Item) -> Self::Output {
        Self {
            health: self.health,
            damage: self.damage + item.damage,
            armour: self.armour + item.armour,
        }
    }
}

#[derive(Copy, Clone)]
struct Item {
    cost: i32,
    damage: i32,
    armour: i32,
}
impl Add for Item {
    type Output = Self;
    fn add(self, other: Self) -> Self::Output {
        Self {
            cost: self.cost + other.cost,
            damage: self.damage + other.damage,
            armour: self.armour + other.armour,
        }
    }
}
