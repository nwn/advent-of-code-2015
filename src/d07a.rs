use std::io::prelude::*;
use std::collections::HashMap;
pub fn main(input: impl BufRead) {
    let mut circuit = HashMap::new();
    for line in input.lines().map(Result::unwrap) {
        let words: Vec<_> = line.split_whitespace().collect();

        let (src, dst);
        if words[1] == "->" {
            dst = words[2].to_string();
            src = Id(words[0].to_string());
        } else if words[0] == "NOT" {
            dst = words[3].to_string();
            src = Not(words[1].to_string());
        } else if words[1] == "AND" {
            dst = words[4].to_string();
            src = And(words[0].to_string(), words[2].to_string());
        } else if words[1] == "OR" {
            dst = words[4].to_string();
            src = Or(words[0].to_string(), words[2].to_string());
        } else if words[1] == "LSHIFT" {
            dst = words[4].to_string();
            src = LShift(words[0].to_string(), words[2].parse().unwrap());
        } else if words[1] == "RSHIFT" {
            dst = words[4].to_string();
            src = RShift(words[0].to_string(), words[2].parse().unwrap());
        } else {
            panic!("Unknown gate");
        }

        circuit.insert(dst, src);
    }

    let mut eval = Evaluator::new(&circuit);
    println!("{}", eval.get_value("a"));
}

type Wire = String;
#[derive(Debug)]
enum Gate {
    Id(Wire),
    Not(Wire),
    And(Wire, Wire),
    Or(Wire, Wire),
    LShift(Wire, u8),
    RShift(Wire, u8),
}
use self::Gate::*;

struct Evaluator<'a> {
    circuit: &'a HashMap<Wire, Gate>,
    memo: HashMap<Wire, u16>,
}
impl<'a> Evaluator<'a> {
    fn new(circuit: &'a HashMap<Wire, Gate>) -> Self {
        Self {
            circuit,
            memo: HashMap::new(),
        }
    }
    fn get_value(&mut self, wire: &str) -> u16 {
        if let Ok(value) = wire.parse() {
            return value;
        }
        if let Some(value) = self.memo.get(&wire.to_string()) {
            return *value;
        }
        let value = match &self.circuit[wire] {
            Id(a) => self.get_value(a),
            Not(a) => !self.get_value(a),
            And(a,b) => self.get_value(a) & self.get_value(b),
            Or(a,b) => self.get_value(a) | self.get_value(b),
            LShift(a,n) => self.get_value(a) << n,
            RShift(a,n) => self.get_value(a) >> n,
        };
        self.memo.insert(wire.to_string(), value);
        value
    }
}
