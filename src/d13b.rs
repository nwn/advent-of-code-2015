use std::io::prelude::*;
use std::collections::HashMap;
pub fn main(input: impl BufRead) {
    let mut happiness = HashMap::new();
    happiness.insert("Self".to_string(), HashMap::new());
    for line in input.lines().map(Result::unwrap) {
        let words: Vec<_> = line.split_whitespace().collect();
        let subject = words[0].to_string();
        let object = words[10].split_terminator('.').next().unwrap().to_string();
        let amount = (if words[2] == "gain" { 1 } else { -1 }) * (words[3].parse::<i32>().unwrap());
        happiness.get_mut("Self").unwrap().insert(object.clone(), 0);
        happiness.entry(subject.clone()).or_insert(HashMap::new()).insert("Self".to_string(), 0);
        happiness.entry(subject).or_insert(HashMap::new()).insert(object, amount);
    }

    let order: Vec<_> = happiness.keys().collect();

    let mut best_sum = 0;
    for order in Permutation::new(&order) {
        let mut sum = 0;
        for i in 0..order.len() {
            sum += happiness[*order[i]][*order[(i+1)%order.len()]];
            sum += happiness[*order[(i+1)%order.len()]][*order[i]];
        }
        if sum > best_sum {
            best_sum = sum;
        }
    }

    println!("{}", best_sum);
}

/// This generates all permutations of the given slice, in order
/// as if the original elements were lexicographically sorted.
///
/// The algorithm used is the generation of Lehmer codes using
/// the factorial number system. This is quite slow due to the
/// arbitrary insertion and removal to adjust indices. Faster
/// algorithms could be swapped in.
struct Permutation<'a, T: 'a> {
    fact: Vec<usize>,
    counter: usize,
    values: &'a [T],
    len: usize,
}
impl<'a, T> Permutation<'a, T> {
    fn new(values: &'a [T]) -> Self {
        // Precompute factorials
        let mut fact = vec![1];
        for i in 1..=values.len() {
            let value = i * fact[i - 1];
            fact.push(value);
        }
        Self {
            fact,
            counter: 0,
            values,
            len: values.len(),
        }
    }
}
impl<'a, T> Iterator for Permutation<'a, T> {
    type Item = Vec<&'a T>;
    fn next(&mut self) -> Option<Self::Item> {
        if self.counter == self.fact[self.len] {
            return None;
        }

        let mut vec: Vec<_> = self.values.iter().collect();
        for i in 0..self.len-1 {
            let offset = (self.counter % self.fact[self.len - i]) / self.fact[self.len - i - 1];
            let item = vec.remove(i + offset);
            vec.insert(i, item);
        }

        self.counter += 1;
        Some(vec)
    }
}
