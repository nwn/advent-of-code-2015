use std::io::prelude::*;
pub fn main(input: impl BufRead) {
    for line in input.lines().map(Result::unwrap) {
        println!("{}", line);
    }
}
