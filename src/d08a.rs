use std::io::prelude::*;
pub fn main(input: impl BufRead) {
    let mut diff = 0;
    for line in input.lines().map(Result::unwrap) {
        let mut chars = line.chars().peekable();
        while let Some(ch) = chars.next() {
            // Skip unescaped quotes (only at start and end)
            if ch == '"' {
                diff += 1;
            }

            // Skip escaped characters
            if ch == '\\' {
                match chars.peek().unwrap() {
                    '\\' => {
                        chars.next();
                        diff += 1;
                    },
                    '\"' => {
                        chars.next();
                        diff += 1;
                    },
                    'x' => {
                        chars.next(); // skip x
                        chars.next(); // skip hi
                        chars.next(); // skip lo
                        diff += 3;
                    },
                    _ => (),
                }
            }
        }
    }
    println!("{}", diff);
}
