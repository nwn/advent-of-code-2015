use std::io::prelude::*;
use std::collections::HashMap;
pub fn main(input: impl BufRead) {
    let mut set = vec![];
    for line in input.lines().map(std::result::Result::unwrap) {
        set.push(line.parse::<i64>().unwrap());
    }

    let parts = 4;
    println!("{}", solve(&set, parts));
}

// Algorithm: Since 4-way partitioning becomes extremely slow, we instead try
// all possible lengths L for the smallest partition, from 1 to |S|. For each
// partition length L, we try all possible combinations of L elements of S. If
// the combination has the expected sum, we test if the remaining elements form
// a 3-way partition and keep the least product of these combinations.
//
// As a significant optimization, only do the 3-way partition test for
// combinations that would provide a lower product than the previous best.
// Since the combinations are iterated in lexicographical order and the set is
// sorted, the best product is likely to be found early on.
fn solve(set: &[i64], parts: usize) -> i64 {
    fn for_combinations(set: &[i64], part: &mut Vec<usize>, func: &mut FnMut(&[i64], &[i64])) {
        if part.len() == part.capacity() {
            let mut comb = Vec::with_capacity(part.len());
            let mut rest = Vec::with_capacity(set.len() - part.len());
            let mut next_part = 0;
            for i in 0..set.len() {
                if Some(&i) == part.get(next_part) {
                    comb.push(set[i]);
                    next_part += 1;
                } else {
                    rest.push(set[i]);
                }
            }

            func(&comb, &rest);
        } else {
            let next = if part.is_empty() { 0 } else { part.last().unwrap() + 1 };
            for i in next..set.len() {
                part.push(i);
                for_combinations(set, part, func);
                part.pop();
            }
        }
    }

    let total = set.iter().sum::<i64>();
    let part_sum = total / parts as i64;
    let mut best = std::i64::MAX;
    for part_size in 1..set.len() {
        for_combinations(set, &mut Vec::with_capacity(part_size),
            &mut |comb, rest| {
                let sum = comb.iter().sum::<i64>();
                let prod = comb.iter().product();
                if sum == part_sum && prod < best {
                    if Partition3::new().solve(rest, part_sum, part_sum) {
                        best = prod;
                    }
                }
            }
        );
        if best < std::i64::MAX {
            break;
        }
    }

    best
}

// Dynamic programming solution to 3-way partition problem.
//
// This builds off of the common pseudo-polynomial DP solution to the partition
// problem, extending it to partition into 3 sets of equal sum.
//
// Recurrence relation:
// Let f(i, n, m) iff there are two disjoint sets in S[0,i) with sums n and m.
// Then f(i, n, m) = f(i-1, n, m) || f(i-1, n-S[i], m) || f(i-1, n, m-S[i]).
struct Partition3 {
    memo: HashMap<(usize, i64, i64), bool>,
}
impl Partition3 {
    fn new() -> Self {
        Self { memo: HashMap::new() }
    }
    fn solve(&mut self, set: &[i64], sum1: i64, sum2: i64) -> bool {
        if set.is_empty() || sum1 < 0 || sum2 < 0 {
            sum1 == 0 && sum2 == 0
        } else if let Some(&ans) = self.memo.get(&(set.len(), sum1, sum2)) {
            ans
        } else {
            let i = set.len() - 1;
            let ans = self.solve(&set[..i], sum1, sum2) ||
                      self.solve(&set[..i], sum1 - set[i], sum2) ||
                      self.solve(&set[..i], sum1, sum2 - set[i]);
            self.memo.insert((set.len(), sum1, sum2), ans);
            ans
        }
    }
}
