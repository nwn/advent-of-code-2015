use std::fs::File;
use std::io::BufReader;

mod d00a;
mod d01a;
mod d01b;
mod d02a;
mod d02b;
mod d03a;
mod d03b;
mod d04a;
mod d04b;
mod d05a;
mod d05b;
mod d06a;
mod d06b;
mod d07a;
mod d07b;
mod d08a;
mod d08b;
mod d09a;
mod d09b;
mod d10a;
mod d10b;
mod d11a;
mod d11b;
mod d12a;
mod d12b;
mod d13a;
mod d13b;
mod d14a;
mod d14b;
mod d15a;
mod d15b;
mod d16a;
mod d16b;
mod d17a;
mod d17b;
mod d18a;
mod d18b;
mod d19a;
mod d19b;
mod d20a;
mod d20b;
mod d21a;
mod d21b;
mod d22a;
mod d22b;
mod d23a;
mod d23b;
mod d24a;
mod d24b;
mod d25a;
mod d25b;

fn main() -> std::io::Result<()> {
    let day = std::env::args().nth(1).unwrap();

    let input = format!("inputs/{}", day);
    let input = File::open(&input).expect("No input file found");
    let input = BufReader::new(input);

    match day.as_str() {
        "00a" => d00a::main(input),
        "01a" => d01a::main(input),
        "01b" => d01b::main(input),
        "02a" => d02a::main(input),
        "02b" => d02b::main(input),
        "03a" => d03a::main(input),
        "03b" => d03b::main(input),
        "04a" => d04a::main(input),
        "04b" => d04b::main(input),
        "05a" => d05a::main(input),
        "05b" => d05b::main(input),
        "06a" => d06a::main(input),
        "06b" => d06b::main(input),
        "07a" => d07a::main(input),
        "07b" => d07b::main(input),
        "08a" => d08a::main(input),
        "08b" => d08b::main(input),
        "09a" => d09a::main(input),
        "09b" => d09b::main(input),
        "10a" => d10a::main(input),
        "10b" => d10b::main(input),
        "11a" => d11a::main(input),
        "11b" => d11b::main(input),
        "12a" => d12a::main(input),
        "12b" => d12b::main(input),
        "13a" => d13a::main(input),
        "13b" => d13b::main(input),
        "14a" => d14a::main(input),
        "14b" => d14b::main(input),
        "15a" => d15a::main(input),
        "15b" => d15b::main(input),
        "16a" => d16a::main(input),
        "16b" => d16b::main(input),
        "17a" => d17a::main(input),
        "17b" => d17b::main(input),
        "18a" => d18a::main(input),
        "18b" => d18b::main(input),
        "19a" => d19a::main(input),
        "19b" => d19b::main(input),
        "20a" => d20a::main(input),
        "20b" => d20b::main(input),
        "21a" => d21a::main(input),
        "21b" => d21b::main(input),
        "22a" => d22a::main(input),
        "22b" => d22b::main(input),
        "23a" => d23a::main(input),
        "23b" => d23b::main(input),
        "24a" => d24a::main(input),
        "24b" => d24b::main(input),
        "25a" => d25a::main(input),
        "25b" => d25b::main(input),
        _ => panic!("Invalid day {}", day),
    }

    Ok(())
}
