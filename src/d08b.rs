use std::io::prelude::*;
pub fn main(input: impl BufRead) {
    let mut diff = 0;
    for line in input.lines().map(Result::unwrap) {
        let mut chars = line.chars();
        while let Some(ch) = chars.next() {
            if ch == '"' || ch == '\\' {
                diff += 1;
            }
        }

        // Count new start and end quotes
        diff += 2;
    }
    println!("{}", diff);
}
