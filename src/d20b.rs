use std::io::prelude::*;
pub fn main(input: impl BufRead) {
    let target = input.lines().next().unwrap().unwrap().parse::<u64>().unwrap();

    // No fancy formulae this time. Just brute force it
    // with a large preallocated array.
    let last = target as usize / 10;
    let mut counts = vec![0; last];
    for i in 1..=last {
        for j in 1..=std::cmp::min(50, last / i) {
            counts[i * j - 1] += i as u64 * 11;
        }
        if counts[i - 1] >= target {
            println!("{}", i);
            break;
        }
    }
}
