use std::io::prelude::*;
pub fn main(input: impl BufRead) {
    let mut password = [0; 8];
    password.copy_from_slice(input.lines().next().unwrap().unwrap().as_bytes());
    next_password(&mut password);
    next_password(&mut password);

    println!("{}", to_str(&password));
}

fn next_password(password: &mut [u8; 8]) {
    loop {
        increment(password, 7);

        // Check for run
        let has_run = password.windows(3).any(|triple| {
            triple[0] + 1 == triple[1] && triple[1] + 1 == triple[2]
        });

        // Check for double letters
        let has_doubles = {
            let mut first = None;
            let mut last = None;
            for (i, pair) in password.windows(2).enumerate() {
                if pair[0] == pair[1] {
                    if first.is_none() {
                        first = Some(i);
                    }
                    last = Some(i);
                }
            }
            if let (Some(a), Some(b)) = (first, last) {
                a + 2 <= b
            } else {
                false
            }
        };

        if has_run && has_doubles {
            break;
        }
    }
}

fn increment(word: &mut [u8; 8], pos: usize) {
    if pos >= 8 {
        *word = [0; 8];
    } else if word[pos] == b'z' {
        increment(word, pos - 1);
    } else {
        word[pos] += 1;
        for ch in &mut word[pos+1..] {
            *ch = b'a';
        }

        // Increment past illegal letters
        if word[pos] == b'i' || word[pos] == b'o' || word[pos] == b'l' {
            increment(word, pos);
        }
    }
}

fn to_str<'a>(word: &'a[u8; 8]) -> &'a str {
    std::str::from_utf8(word).unwrap()
}
