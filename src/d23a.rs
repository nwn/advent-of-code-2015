use std::io::prelude::*;
pub fn main(input: impl BufRead) {
    let mut program = vec![];
    for line in input.lines().map(Result::unwrap) {
        let words: Vec<_> = line.split_whitespace().collect();
        let instr = match words[0] {
            "hlf" => Hlf(words[1].starts_with("a")),
            "tpl" => Tpl(words[1].starts_with("a")),
            "inc" => Inc(words[1].starts_with("a")),
            "jmp" => Jmp(words[1].parse().unwrap()),
            "jie" => Jie(words[1].starts_with("a"), words[2].parse().unwrap()),
            "jio" => Jio(words[1].starts_with("a"), words[2].parse().unwrap()),
            _ => panic!("Encountered unknown instruction {}", words[0]),
        };
        program.push(instr);
    }

    let mut a: u64 = 0;
    let mut b: u64 = 0;
    let mut pc = 0;
    loop {
        if pc as usize >= program.len() {
            break;
        }

        let mut step = 1;
        match program[pc as usize] {
            Hlf(reg) => if reg { a /= 2; } else { b /= 2; },
            Tpl(reg) => if reg { a *= 3; } else { b *= 3; },
            Inc(reg) => if reg { a += 1; } else { b += 1; },
            Jmp(n)   => step = n,
            Jie(reg, n) => {
                let reg = if reg { a } else { b };
                if reg % 2 == 0 { step = n; }
            },
            Jio(reg, n) => {
                let reg = if reg { a } else { b };
                if reg == 1 { step = n; }
            },
        }
        pc += step;
    }

    println!("{}", b);
}

enum Instr {
    Hlf(bool),
    Tpl(bool),
    Inc(bool),
    Jmp(isize),
    Jie(bool, isize),
    Jio(bool, isize),
}
use self::Instr::*;
