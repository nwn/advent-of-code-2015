use std::io::prelude::*;
pub fn main(input: impl BufRead) {
    let mut grid = vec![];
    for line in input.lines().map(Result::unwrap) {
        let mut row = vec![];
        for ch in line.chars() {
            row.push(ch == '#');
        }
        grid.push(row);
    }
    light_corners(&mut grid);

    // Take 100 steps of Conway's Game of Life
    let mut new_grid = grid.clone();
    for _ in 0..100 {
        for row in 0..grid.len() {
            for col in 0..grid[row].len() {
                let mut count = 0;
                for i in 0..3 {
                    if row == 0 && i == 0 {
                        continue;
                    } else if row + 1 == grid.len() && i + 1 == 3 {
                        continue;
                    }
                    for j in 0..3 {
                        if col == 0 && j == 0 {
                            continue;
                        } else if col + 1 == grid[i].len() && j + 1 == 3 {
                            continue;
                        }

                        if i == 1 && j == 1 {
                            continue;
                        }
                        if grid[row + i - 1][col + j - 1] {
                            count += 1;
                        }
                    }
                }

                if grid[row][col] {
                    new_grid[row][col] = count == 2 || count == 3;
                } else {
                    new_grid[row][col] = count == 3;
                }
            }
        }
        light_corners(&mut new_grid);
        std::mem::swap(&mut grid, &mut new_grid);
    }

    let mut count = 0;
    for row in &grid {
        for cell in row {
            if *cell {
                count += 1;
            }
       }
    }
    println!("{}", count);
}

fn light_corners(grid: &mut Vec<Vec<bool>>) {
    let rows = grid.len() - 1;
    let cols = grid[0].len() - 1;
    grid[0][0] = true;
    grid[0][cols] = true;
    grid[rows][0] = true;
    grid[rows][cols] = true;
}
