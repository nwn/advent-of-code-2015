use std::io::prelude::*;
pub fn main(input: impl BufRead) {
    let mut words: Vec<_> = input.lines().map(Result::unwrap).next().unwrap().split_whitespace().map(str::to_owned).collect();
    words = words.into_iter().map(|word| word.split_terminator(|ch: char| ch.is_ascii_punctuation()).next().unwrap().to_owned()).collect();
    let mut nums = words.iter().filter_map(|str| str.parse::<u64>().ok());
    let row = nums.next().unwrap();
    let col = nums.next().unwrap();
    let diag = row + col - 1;

    let diag_steps = diag * (diag - 1) / 2;
    let col_steps = col - 1;

    println!("{}", step(20151125, diag_steps + col_steps));
}

fn step(mut n: u64, times: u64) -> u64 {
    for _ in 0..times {
        n *= 252533;
        n %= 33554393;
    }
    n
}
