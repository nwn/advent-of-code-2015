use std::io::prelude::*;
pub fn main(input: impl BufRead) {
    let mut sum = 0;
    for line in input.lines().map(Result::unwrap) {
        let mut line = &line[..];

        fn is_numeric(ch: char) -> bool { ch.is_ascii_digit() || ch == '-' }
        while let Some(start) = line.find(is_numeric) {
            let end = start + line.get(start..).unwrap().find(|ch| !is_numeric(ch)).unwrap();
            sum += line.get(start..end).unwrap().parse::<i32>().unwrap();
            line = line.get(end..).unwrap();
        }
    }

    println!("{}", sum);
}
