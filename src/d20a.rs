use std::io::prelude::*;
pub fn main(input: impl BufRead) {
    let target = input.lines().next().unwrap().unwrap().parse::<u64>().unwrap();
    for i in 1.. {
        let sum_of_divisors = sum_of_divisors(i);
        if 10 * sum_of_divisors >= target {
            println!("{}", i);
            break;
        }
    }
}

fn prime_factorization(mut n: u64) -> Vec<(u64,u64)> {
    let mut factors = vec![];

    let mut i = 2;
    let mut cur = (i,0);
    loop {
        if n % i == 0 {
            cur.1 += 1;
            n /= i;
        } else {
            if cur.1 > 0 { factors.push(cur); }
            i += 1;
            cur = (i,0);
            if i * i > n { break; }
        }
    }
    if n > 1 {
        factors.push((n, 1));
    }

    factors
}

// Sum of divisors, given prime factorization: n = Product[p_i ^ m_i, {i,1,k}].
// Sum of all divisors = Product[(p_i**(m_i+1) - 1) / (p_i - 1), {i,1,k}].
fn sum_of_divisors(n: u64) -> u64 {
    let mut total = 1;
    for (factor, exp) in prime_factorization(n) {
        let mut p = 1;
        for _ in 0..=exp {
            p *= factor;
        }
        total *= (p - 1) / (factor - 1);
    }
    total
}
