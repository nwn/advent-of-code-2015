use std::io::prelude::*;
pub fn main(input: impl BufRead) {
    let mut molecule = String::from("");
    for line in input.lines().map(Result::unwrap) {
        let words: Vec<_> = line.split_whitespace().collect();
        if words.len() == 1 {
            molecule = words[0].to_owned();
            break;
        }
    }
    let atoms = split_molecule(&molecule);

    // Treat this as a grammar that we are parsing to the root symbol e.
    // From inspection, the production rules fall into 3 categories:
    //  e => XX
    //  X => XX
    //  X => X Rn X (Y X)* Ar
    // where X represents any atom except Rn, Y, and Ar.
    // Then to reduce a production, types 1 and 2 decrease the total length
    // by exactly 1. For type 3, the "Rn X (Y X)* Ar" symbols are removed,
    // adding to #Rn + #Ar + 2*#Y.
    // Therefore to reduce the molecule to a single e requires:
    //  #X - #Y - 1 steps.

    let mut num_x = 0;
    let mut num_y = 0;
    for atom in atoms {
        match atom {
            "Rn" | "Ar" => (),
            "Y"  => num_y += 1,
            _ => num_x += 1,
        }
    }
    println!("{}", num_x - num_y - 1);
}

fn split_molecule<'a>(mut mol: &'a str) -> Vec<&'a str> {
    let mut vec = vec![];
    while !mol.is_empty() {
        if let Some(end) = mol[1..].find(char::is_uppercase) {
            vec.push(&mol[..end+1]);
            mol = &mol[end+1..];
        } else {
            vec.push(mol);
            break;
        }
    }
    vec
}
