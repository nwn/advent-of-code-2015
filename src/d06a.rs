use std::io::prelude::*;
pub fn main(input: impl BufRead) {
    let mut grid = vec![vec![false; 1000]; 1000];
    for line in input.lines().map(Result::unwrap) {
        let words: Vec<_> = line.split_whitespace().collect();
        let cmd;
        let (start, end);
        match words[1] {
            "on" => {
                cmd = On;
                start = words[2];
                end = words[4];
            },
            "off" => {
                cmd = Off;
                start = words[2];
                end = words[4];
            },
            _ => {
                cmd = Toggle;
                start = words[1];
                end = words[3];
            },
        };
        let mut start = start.split(',');
        let x1: usize = start.next().unwrap().parse().unwrap();
        let y1: usize = start.next().unwrap().parse().unwrap();
        let mut end = end.split(',');
        let x2: usize = end.next().unwrap().parse().unwrap();
        let y2: usize = end.next().unwrap().parse().unwrap();

        for x in x1..=x2 {
            for y in y1..=y2 {
                grid[x][y] = match cmd {
                    On => true,
                    Off => false,
                    Toggle => !grid[x][y],
                };
            }
        }
    }

    let mut count = 0;
    for row in grid {
        for cell in row {
            if cell {
                count += 1;
            }
        }
    }
    println!("{}", count);
}

enum Cmd { On, Off, Toggle }
use self::Cmd::*;
