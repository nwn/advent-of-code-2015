use std::io::prelude::*;
pub fn main(input: impl BufRead) {
    let mut count = 0;
    for line in input.lines().map(Result::unwrap) {
        if is_nice(line.as_bytes()) {
            count += 1;
        }
    }
    println!("{}", count);
}

fn is_nice(bytes: &[u8]) -> bool {
    {   // Check vowels
        let is_vowel = |ch: &&u8| "aeiou".contains(**ch as char);
        if bytes.iter().filter(is_vowel).count() < 3 {
            return false;
        }
    }
    {   // Check double letters
        let is_double = |pair: &[u8]| pair[0] == pair[1];
        if !bytes.windows(2).any(is_double) {
            return false;
        }
    }
    {   // Check illegal substrings
        let is_illegal = |pair: &[u8]|
            ["ab".as_bytes(), "cd".as_bytes(), "pq".as_bytes(), "xy".as_bytes()].contains(&pair);
        if bytes.windows(2).any(is_illegal) {
            return false;
        }
    }
    true
}
