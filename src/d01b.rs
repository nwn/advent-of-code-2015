use std::io::prelude::*;
pub fn main(input: impl BufRead) {
    let mut floor = 0;
    for line in input.lines().map(Result::unwrap) {
        for (i, byte) in line.bytes().enumerate() {
            match byte {
                b'(' => floor += 1,
                b')' => floor -= 1,
                _ => (),
            }
            if floor < 0 {
                println!("{}", i + 1);
                return;
            }
        }
    }
}
