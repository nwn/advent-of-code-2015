use std::io::prelude::*;
use std::cmp::max;
pub fn main(input: impl BufRead) {
    let mut ingredients = vec![];
    for line in input.lines().map(Result::unwrap) {
        let words: Vec<_> = line.split_whitespace().map(|str| str.split_terminator(',')).flatten().collect();
        let capacity   = words[2].parse::<i32>().unwrap();
        let durability = words[4].parse::<i32>().unwrap();
        let flavour    = words[6].parse::<i32>().unwrap();
        let texture    = words[8].parse::<i32>().unwrap();
        let calories   = words[10].parse::<i32>().unwrap();
        ingredients.push((capacity, durability, flavour, texture, calories));
    }

    let sum = 100;
    let parts = ingredients.len();
    let mut max_score = 0;
    compositions(&mut vec![0; parts], sum, parts,
        &mut |arr| {
            let mut capacity = 0;
            let mut durability = 0;
            let mut flavour = 0;
            let mut texture = 0;
            let mut calories = 0;
            for i in 0..arr.len() {
                capacity += arr[i] * ingredients[i].0;
                durability += arr[i] * ingredients[i].1;
                flavour += arr[i] * ingredients[i].2;
                texture += arr[i] * ingredients[i].3;
                calories += arr[i] * ingredients[i].4;
            }
            if calories == 500 {
                capacity = max(capacity, 0);
                durability = max(durability, 0);
                flavour = max(flavour, 0);
                texture = max(texture, 0);
                let score = capacity * durability * flavour * texture;
                max_score = max(max_score, score);
            }
        }
    );

    println!("{}", max_score);
}

// This function iterates over all integer compositions of `sum` with `parts`
// parts, and calls `func` on each composition. The number of such compositions
// is Binom[sum-1, parts-1].
fn compositions(arr: &mut [i32], sum: i32, parts: usize, func: &mut FnMut(&[i32])) {
    let offset = arr.len() - parts;
    if parts == 1 {
        arr[offset] = sum;
        func(arr);
    } else {
        let max = sum - (parts as i32 - 1);
        for part in (1..=max).rev() {
            arr[offset] = part;
            compositions(arr, sum - part, parts - 1, func);
        }
    }
}
