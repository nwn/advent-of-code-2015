use std::io::prelude::*;
pub fn main(input: impl BufRead) {
    let mut count = 0;
    for line in input.lines().map(Result::unwrap) {
        if is_nice(line.as_bytes()) {
            count += 1;
        }
    }
    println!("{}", count);
}

fn is_nice(bytes: &[u8]) -> bool {
    {   // Check duplicate pairs
        let mut good = false;
        let len = bytes.len()-1;
        'outer: for i in 0..len {
            for j in (i+2)..len {
                if bytes[i..i+2] == bytes[j..j+2] {
                    good = true;
                    break 'outer;
                }
            }
        }
        if !good {
            return false;
        }
    }
    {   // Check almost-double letters
        let is_almost_double = |triple: &[u8]| triple[0] == triple[2];
        if !bytes.windows(3).any(is_almost_double) {
            return false;
        }
    }
    true
}
