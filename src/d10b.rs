use std::io::prelude::*;
pub fn main(input: impl BufRead) {
    let mut seq = to_byte_vec(&input.lines().next().unwrap().unwrap());

    for _ in 0..50 {
        seq = {
            let mut new_seq = vec![];
            let mut iter = seq.iter().peekable();
            while let Some(ch) = iter.next() {
                let mut count = 1;
                while iter.peek() == Some(&ch) {
                    iter.next();
                    count += 1;
                }
                new_seq.append(&mut to_byte_vec(&format!("{}", count)));
                new_seq.push(*ch);
            }
            new_seq
        };
    }

    println!("{}", seq.len());
}

fn to_byte_vec(seq: &str) -> Vec<u8> {
    seq.as_bytes().iter().map(|ch| ch - b'0').collect()
}
