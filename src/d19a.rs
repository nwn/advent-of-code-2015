use std::io::prelude::*;
use std::collections::{HashSet, HashMap};
pub fn main(input: impl BufRead) {
    let mut prods = HashMap::new();
    let mut molecule = String::from("");
    for line in input.lines().map(Result::unwrap) {
        let words: Vec<_> = line.split_whitespace().collect();
        if words.len() == 0 {
            continue;
        } else if words.len() == 1 {
            molecule = words[0].to_owned();
            continue;
        }

        let from = words[0].to_owned();
        let to = words[2].to_owned();

        prods.entry(from).or_insert(vec![]).push(to);
    }

    let atoms = split_molecule(&molecule);
    let mut molecules = HashSet::new();
    for i in 0..atoms.len() {
        let prefix = atoms[0..i].join("");
        let suffix = atoms[i+1..].join("");
        if let Some(replacements) = prods.get(atoms[i]) {
            for repl in replacements {
                let mut molecule = prefix.clone();
                molecule.push_str(repl);
                molecule.push_str(&suffix);
                molecules.insert(molecule);
            }
        }
    }
    println!("{}", molecules.len());
}

fn split_molecule<'a>(mut mol: &'a str) -> Vec<&'a str> {
    let mut vec = vec![];
    while !mol.is_empty() {
        if let Some(end) = mol[1..].find(char::is_uppercase) {
            vec.push(&mol[..end+1]);
            mol = &mol[end+1..];
        } else {
            vec.push(mol);
            break;
        }
    }
    vec
}
