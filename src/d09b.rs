use std::io::prelude::*;
use std::collections::HashMap;
pub fn main(input: impl BufRead) {
    let lines: Vec<_> = input.lines().map(Result::unwrap).collect();

    // Map city names to indices
    let mut city_ids = HashMap::new();
    for line in &lines {
        let words: Vec<_> = line.split_whitespace().collect();
        let mut push_city = |name: &str|
            if !city_ids.contains_key(name) {
                let id = city_ids.len();
                city_ids.insert(name.to_string(), id);
            }
        ;
        push_city(words[0]);
        push_city(words[2]);
    }
    let num_cities = city_ids.len();

    // Build adjacency list
    let mut neighbours = vec![vec![]; num_cities];
    for line in &lines {
        let words: Vec<_> = line.split_whitespace().collect();
        let a = city_ids[words[0]];
        let b = city_ids[words[2]];
        let dist: u32 = words[4].parse().unwrap();

        neighbours[a].push((b, dist));
        neighbours[b].push((a, dist));
    }
    // Push a global neighbour as the starting location
    neighbours.push({
        let mut vec = vec![];
        for id in 0..num_cities {
            vec.push((id, 0));
        }
        vec
    });

    let start = num_cities;
    let unvisited = (1u64 << num_cities) - 1;
    let solution = solve(&neighbours, start, unvisited);

    println!("{}", solution);
}

fn solve(neighbours: &Vec<Vec<(usize, u32)>>, cur: usize, unvisited: u64) -> u32 {
    if unvisited == 0 {
        // No more unvisited cities
        return 0;
    }

    let mut distances = vec![];
    for (id, dist) in &neighbours[cur] {
        // Skip already visited cities
        if (unvisited & (1u64 << id)) == 0 { continue; }

        distances.push(dist + solve(neighbours, *id, unvisited & (!(1u64 << id))));
    }

    distances[..].sort();
    distances[distances.len()-1]
}
