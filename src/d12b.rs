extern crate json;
use std::io::prelude::*;
pub fn main(input: impl BufRead) {
    let line = input.lines().next().unwrap().unwrap();
    let val = json::parse(&line).unwrap();

    println!("{}", sum_values(&val));
}

fn sum_values(val: &json::JsonValue) -> i32 {
    use self::json::JsonValue::*;
    match val {
        Number(num) => (*num).into(),
        Array(vec) => vec.iter().map(sum_values).sum(),
        Object(obj) => {
            if obj.iter().any(|(id, val)| id == "red" || val.as_str() == Some("red")) {
                0
            } else {
                obj.iter().map(|(_, val)| sum_values(val)).sum()
            }
        },
        _ => 0,
    }
}
