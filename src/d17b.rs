use std::io::prelude::*;
use std::collections::HashMap;
pub fn main(input: impl BufRead) {
    let mut arr = vec![];
    for line in input.lines().map(Result::unwrap) {
        arr.push(line.parse::<i32>().unwrap());
    }

    let mut memo = Memo::new();
    println!("{}", memo.solve(&arr, 150).1);
}

// This is a counting version of the subset sum problem, which is NP-hard.
// Dynamic programming using top-down memoization speeds this up significantly.
struct Memo {
    memo: HashMap<(usize, i32), (usize, u32)>,
}
impl Memo {
    fn new() -> Self {
        Self { memo: HashMap::new() }
    }
    fn solve(&mut self, arr: &[i32], sum: i32) -> (usize, u32) {
        if sum == 0 {
            return (arr.len(), 1);
        }
        if arr.is_empty() || sum < 0 {
            return (0, 0);
        }
        if let Some(ans) = self.memo.get(&(arr.len(), sum)) {
            return *ans;
        }

        let take = self.solve(&arr[1..], sum - arr[0]);
        let mut leave = self.solve(&arr[1..], sum);
        if leave.1 > 0 { leave.0 += 1; }
        let better;
        if take.0 > leave.0 {
            better = take;
        } else if take.0 < leave.0 {
            better = leave;
        } else {
            better = (take.0, take.1 + leave.1);
        }

        self.memo.insert((arr.len(), sum), better);
        better
    }
}
