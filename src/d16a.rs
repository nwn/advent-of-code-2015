use std::io::prelude::*;
use std::collections::{HashSet, HashMap};
pub fn main(input: impl BufRead) {
    let mut sues = vec![];
    for line in input.lines().map(Result::unwrap) {
        let words: Vec<_> = line.split_whitespace()
                                .flat_map(|str| str.split_terminator(|ch: char| ch.is_ascii_punctuation())).collect();
        let mut sue = HashMap::new();
        for i in (2..words.len()).step_by(2) {
            sue.insert(words[i].to_owned(), words[i + 1].parse::<u32>().unwrap());
        }
        sues.push(sue);
    }

    let compounds = [
        ("children".to_string(),    3),
        ("cats".to_string(),        7),
        ("samoyeds".to_string(),    2),
        ("pomeranians".to_string(), 3),
        ("akitas".to_string(),      0),
        ("vizslas".to_string(),     0),
        ("goldfish".to_string(),    5),
        ("trees".to_string(),       3),
        ("cars".to_string(),        2),
        ("perfumes".to_string(),    1),
    ];

    let mut candidates: HashSet<_> = (0..sues.len()).collect();
    for i in 0..sues.len() {
        for (name, num) in compounds.iter() {
            if let Some(n) = sues[i].get(name) {
                if n != num {
                    candidates.remove(&i);
                    break;
                }
            }
        }
    }

    println!("{}", candidates.iter().next().unwrap() + 1);
}
