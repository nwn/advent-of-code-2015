use std::io::prelude::*;
pub fn main(input: impl BufRead) {
    let mut floor = 0;
    for line in input.lines().map(Result::unwrap) {
        for byte in line.bytes() {
            match byte {
                b'(' => floor += 1,
                b')' => floor -= 1,
                _ => (),
            }
        }
    }

    println!("{}", floor);
}
