extern crate md5;
use std::io::prelude::*;
pub fn main(input: impl BufRead) {
    let key = input.lines().next().unwrap().unwrap();

    for i in 0.. {
        let key = format!("{}{}", key, i);
        let digest = md5::compute(&key);
        let digest = format!("{:x}", digest);
        if digest.starts_with("00000") {
            println!("{}", i);
            break;
        }
    }
}
