use std::io::prelude::*;
use std::collections::HashSet;
pub fn main(input: impl BufRead) {
    let mut visited = HashSet::new();
    let mut pos = (0, 0);
    visited.insert(pos);
    for line in input.lines().map(Result::unwrap) {
        for ch in line.chars() {
            match ch {
                '^' => pos.1 -= 1,
                'v' => pos.1 += 1,
                '<' => pos.0 -= 1,
                '>' => pos.0 += 1,
                _ => (),
            }
            visited.insert(pos);
        }
    }
    println!("{}", visited.len());
}
