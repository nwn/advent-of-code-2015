use std::io::prelude::*;
pub fn main(input: impl BufRead) {
    let mut grid = vec![vec![0u32; 1000]; 1000];
    for line in input.lines().map(Result::unwrap) {
        let words: Vec<_> = line.split_whitespace().collect();
        let cmd;
        let (start, end);
        match words[1] {
            "on" => {
                cmd = On;
                start = words[2];
                end = words[4];
            },
            "off" => {
                cmd = Off;
                start = words[2];
                end = words[4];
            },
            _ => {
                cmd = Toggle;
                start = words[1];
                end = words[3];
            },
        };
        let mut start = start.split(',');
        let x1: usize = start.next().unwrap().parse().unwrap();
        let y1: usize = start.next().unwrap().parse().unwrap();
        let mut end = end.split(',');
        let x2: usize = end.next().unwrap().parse().unwrap();
        let y2: usize = end.next().unwrap().parse().unwrap();

        for x in x1..=x2 {
            for y in y1..=y2 {
                let cell = &mut grid[x][y];
                match cmd {
                    On => *cell = cell.saturating_add(1),
                    Off => *cell = cell.saturating_sub(1),
                    Toggle => *cell = cell.saturating_add(2),
                }
            }
        }
    }

    let mut sum = 0;
    for row in grid {
        for cell in row {
            sum += cell;
        }
    }
    println!("{}", sum);
}

enum Cmd { On, Off, Toggle }
use self::Cmd::*;
