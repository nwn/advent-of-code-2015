use std::io::prelude::*;
use std::collections::HashSet;
pub fn main(input: impl BufRead) {
    let mut visited = HashSet::new();

    let mut pos1 = (0, 0);
    let mut pos2 = (0, 0);
    visited.insert(pos1);
    for line in input.lines().map(Result::unwrap) {
        for (i, ch) in line.chars().enumerate() {
            let pos = if i % 2 == 0 { &mut pos1 } else { &mut pos2 };
            match ch {
                '^' => pos.1 -= 1,
                'v' => pos.1 += 1,
                '<' => pos.0 -= 1,
                '>' => pos.0 += 1,
                _ => (),
            }
            visited.insert(*pos);
        }
    }
    println!("{}", visited.len());
}
